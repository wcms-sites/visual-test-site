core = 7.x
api = 2

; styleguide
projects[styleguide][type] = "module"
projects[styleguide][download][type] = "git"
projects[styleguide][download][url] = "https://git.uwaterloo.ca/drupal-org/styleguide.git"
projects[styleguide][download][tag] = "7.x-1.1"
projects[styleguide][subdir] = ""

; uw_cfg_visual_test_site
projects[uw_cfg_visual_test_site][type] = "module"
projects[uw_cfg_visual_test_site][download][type] = "git"
projects[uw_cfg_visual_test_site][download][url] = "https://git.uwaterloo.ca/wcms/uw_cfg_visual_test_site.git"
projects[uw_cfg_visual_test_site][download][tag] = "7.x-1.0"
projects[uw_cfg_visual_test_site][subdir] = ""
